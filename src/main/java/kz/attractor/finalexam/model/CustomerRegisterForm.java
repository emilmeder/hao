package kz.attractor.finalexam.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class CustomerRegisterForm {
    @NotBlank
    @Email
    private String email = "";

    @Size(min=4, max=24, message = "Length must be > 4 and <= 24")
    private String name = "";

    @NotBlank
    @Size(min= 5,max = 40 )
    private String password = "";
}

