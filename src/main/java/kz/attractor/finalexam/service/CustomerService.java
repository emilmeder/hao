package kz.attractor.finalexam.service;



import kz.attractor.finalexam.DTO.CustomerResponseDTO;
import kz.attractor.finalexam.exception.CustomerAlreadyRegisteredException;
import kz.attractor.finalexam.model.Customer;
import kz.attractor.finalexam.model.CustomerRegisterForm;
import kz.attractor.finalexam.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepository repository;
    private final PasswordEncoder encoder;
    private final CustomerRepository customerRepository;

    public CustomerResponseDTO register(CustomerRegisterForm form) {
        if (repository.existsByEmail(form.getEmail())) {
            throw new CustomerAlreadyRegisteredException();
        }

        var user = Customer.builder()
                .email(form.getEmail())
                .fullname(form.getName())
                .password(encoder.encode(form.getPassword()))
                .build();

        repository.save(user);

        return CustomerResponseDTO.from(user);
    }
    public Customer getLoginCustomer(Authentication authentication){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var user = customerRepository.findByEmail(userDetails.getUsername());
        return user;
    }
}

