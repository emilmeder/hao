package kz.attractor.finalexam.validator;

public interface DateValidator {
    boolean isValid(String dateStr);
    boolean isAfter(String dateStr, String dateStr2);
    boolean isPersonMature(String birthDate);
}