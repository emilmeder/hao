package kz.attractor.finalexam.validator;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateValidatorUsingDateTimeFormatter implements DateValidator {
    private DateTimeFormatter dateFormatter;

    public DateValidatorUsingDateTimeFormatter(DateTimeFormatter dateFormatter) {
        this.dateFormatter = dateFormatter;
    }

    @Override
    public boolean isValid(String dateStr) {
        try {
            this.dateFormatter.parse(dateStr);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isAfter(String date1, String date2) {
        try {
            LocalDate d1 = LocalDate.parse(date1);
            LocalDate d2 = LocalDate.parse(date2);
            if (d1.isAfter(d2)) {
                return true;
            }
        } catch (DateTimeParseException e) {
            return false;
        }
        return false;
    }

    @Override
    public boolean isPersonMature(String birthDate){
        try {
            LocalDate d1 = LocalDate.parse(birthDate);
            LocalDate now = LocalDate.now();
            Period period = Period.between(d1, now);

            if (period.getYears() >= 16) {
                return true;
            }
        } catch (DateTimeParseException e) {
            return false;
        }
        return false;
    }
}
